module.exports =  {
  env: {
    "node": true
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    'plugin:import/typescript'
  ],
  parser: "@typescript-eslint/parser",
  parserOptions:  {
    ecmaVersion:  2018,
    sourceType:  'module',
  },
  plugins: [
    '@typescript-eslint',
  ],
  rules:  {
    // eslint
    "no-unused-vars": "error",
    "indent": ["error", 2],
    "semi": ["error", "always"],
    "max-params": ["error", 4],
    "no-loop-func": "off",
    "func-style": ["error", "declaration", { "allowArrowFunctions": true }],
    "sort-imports": ["error", {
      "ignoreCase": false,
      "ignoreDeclarationSort": true,
      "ignoreMemberSort": false,
      "memberSyntaxSortOrder": ["none", "all", "multiple", "single"]
    }],

    // typescript-eslint
    "@typescript-eslint/no-for-in-array": "error",
    "@typescript-eslint/no-empty-interface": [
      "error",
      { "allowSingleExtends": true }
    ],
    "@typescript-eslint/promise-function-async": [
      "error",
      {
        "allowedPromiseNames": ["Thenable"],
        "checkArrowFunctions": true,
        "checkFunctionDeclarations": true,
        "checkFunctionExpressions": true,
        "checkMethodDeclarations": true
      }
    ],
    "@typescript-eslint/no-explicit-any": "error",
    "@typescript-eslint/no-inferrable-types": "off",
    "@typescript-eslint/no-loop-func": ["error"],
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        "args": "none",
        "caughtErrors": "none"
      }
    ],
    // style
    "linebreak-style": ["error", "unix"],
    "quotes": ["error", "single"],
    "no-empty": "warn",
    "no-cond-assign": ["error", "always"],
    "for-direction": "error",
    "no-console": "off",

    // experimental
    "no-unused-expression": "off"
  },
  settings: {
    "import/resolver": {
      "typescript": {}
    }
  }
};